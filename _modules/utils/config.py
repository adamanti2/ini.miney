from pydantic import BaseModel
from json import loads, dumps
from os.path import isfile
from io import TextIOWrapper
from typing import Optional, List, Tuple, Dict
from multiprocessing import cpu_count


class ConfigModel(BaseModel):
    cpuCoresAllowed: int = cpu_count() // 2

    webpQuality: int = 90

    alwaysReplaceFiles: bool = False
    imageOutputDir: str = "out/"
    defaultInDir: str = "in/"

    aoImageFormats: List[str] = ["png", "gif", "webp", "apng"]
    aoSoundFormats: List[str] = ["wav", "ogg", "opus"]
    olImageFormats: List[str] = ["png", "gif", "webp", "apng", "jpg", "jpeg"]
    olSoundFormats: List[str] = ["mp3", "wav", "ogg", "opus"]

    aoSoundMap: Dict[str, str] = {
        "https://objection.lol/audio/sound/badum.mp3": "sfx-badum",
        "https://objection.lol/audio/sound/bang.mp3": "sfx-gunshot3",
        "https://objection.lol/audio/sound/damage.mp3": "sfx-damage2",
        "https://objection.lol/audio/sound/damage2.mp3": "sfx-stab2",
        "https://objection.lol/audio/sound/deskslam.mp3": "sfx-deskslam",
        "https://objection.lol/audio/sound/dramapound.mp3": "sfx-dramapound",
        "https://objection.lol/Audio/Sound/earthquake.mp3": "sfx-roar",
        "https://objection.lol/audio/sound/evidence.mp3": "sfx-evidencecollect",
        "https://objection.lol/audio/sound/explosion.mp3": "sfx-explosion",
        "https://objection.lol/audio/sound/fall.mp3": "sfx-thud4",
        "https://objection.lol/audio/sound/flashback.mp3": "sfx-flashback",
        "https://objection.lol/audio/sound/flipbook.mp3": "sfx-pageturn",
        "https://objection.lol/audio/sound/gallery.mp3": "sfx-gallery",
        "https://objection.lol/audio/sound/gallerycheer.mp3": "sfx-gallerycheer",
        "https://objection.lol/audio/sound/gavel.mp3": "sfx-gavel",
        "https://objection.lol/audio/sound/guitar error.mp3": "sfx-guitarrif",
        "https://objection.lol/audio/sound/gunshot.mp3": "sfx-gunshot",
        "https://objection.lol/audio/sound/gunshot2.mp3": "sfx-gunshot4",
        "https://objection.lol/audio/sound/lightbulb.mp3": "sfx-lightbulb",
        "https://objection.lol/audio/sound/photosnap.mp3": "sfx-photosnap",
        "https://objection.lol/audio/sound/realization.mp3": "sfx-realization",
        "https://objection.lol/audio/sound/roar.mp3": "sfx-furio",
        "https://objection.lol/audio/sound/salute.mp3": "sfx-thud2",
        "https://objection.lol/audio/sound/shiny.mp3": "sfx-reddbling",
        "https://objection.lol/audio/sound/shock.mp3": "sfx-badum",
        "https://objection.lol/audio/sound/shooop.mp3": "sfx-evidenceshoop",
        "https://objection.lol/audio/sound/sketch.mp3": "sfx-vera-draw",
        "https://objection.lol/audio/sound/smack.mp3": "sfx-smack",
        "https://objection.lol/audio/sound/snap finger.mp3": "sfx-fwasshing",
        "https://objection.lol/audio/sound/thwap.mp3": "sfx-thwap",
        "https://objection.lol/audio/sound/whack.mp3": "sfx-whack",
        "https://objection.lol/audio/sound/whoops.mp3": "whoops",
        "https://objection.lol/audio/sound/yell.mp3": "sfx-objection",
        "https://objection.lol/audio/sound/yell2.mp3": "sfx-stab"
    }


class _Config:
    writable: bool
    _file: TextIOWrapper
    _config: ConfigModel

    def __init__(self, writable=False):
        self.writable = writable


    def __enter__(self) -> ConfigModel:
        if not isfile("config.json"):
            with open("config.json", "w") as f:
                pass
        
        self._file = open("config.json", "r+" if self.writable else "r")
        content = self._file.read()
        self._config = ConfigModel.parse_raw(content)

        if not self.writable:
            self._file.close()
        
        return self._config
    

    def __exit__(self, exc_type, exc_value, exc_traceback) -> bool:
        if self.writable:
            content = self._config.json(sort_keys=True, indent=2)
            self._file.seek(0)
            self._file.truncate()
            self._file.write(content)
            self._file.close()
        return exc_type is None


def getConfig() -> ConfigModel:
    config = None
    with _Config() as openedConfig:
        config = openedConfig
    assert config is not None
    return config


def writeConfig():
    return _Config(True)
