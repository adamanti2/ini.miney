from ansi.colour import fg, bg, fx
import re
from typing import Iterable, Tuple, Optional


DEFAULT_COLOR = fg.brightyellow
DEFAULT_FORMAT = f"{DEFAULT_COLOR}{fx.underline}"
RESET = str(fx.reset)


def say(*args):
    print(*args)


def padding(lines=1):
    for _ in range(lines):
        say()


def errorMessage(message, errorType = None):
    errorMessage = f"{fg.brightred}! {message}"
    if errorType is not None:
        errorMessage += f" {fg.darkgray}({errorType})"
    errorMessage += f"{fx.reset}"
    say(errorMessage)


def choose(prompt: str, options: Iterable[Tuple[str, str]], default: Optional[int] = None) -> str:
    if prompt != "":
        say(prompt)
    choices = {}
    for i, item in enumerate(options):
        key, title = item
        choice = str(i + 1)
        choices[choice] = key
        message = " - "
        if default == i:
            choices[""] = key
            message += DEFAULT_FORMAT
        message += f"{choice}:"
        if default == i:
            message += str(fx.reset)
        message += f" {title}"
        say(message)

    minOption = min(choices.keys())    
    maxOption = max(choices.keys())    
    choice = input("> ")
    while choice not in choices:
        choice = input(f"Select a valid option ({minOption}-{maxOption}) > ")
    return choices[choice]


def confirm(prompt: str = "", default: Optional[bool] = None) -> bool:
    y = DEFAULT_FORMAT + "Y" + RESET if default == True else "y"
    n = DEFAULT_FORMAT + "N" + RESET if default == False else "n"
    if prompt != "":
        prompt += " "
    choice = input(f"{prompt}({y}/{n}) > ").lower()[:1]
    while choice not in ("y", "n", "1", "0"):
        if choice == "" and default is not None:
            return default
        choice = input(f"{prompt} (Please enter 'y' or 'n') > ").lower()[:1]
    return choice in ("y", "1")


def prompt(prompt: str = "", default: str = "") -> str:
    defaultText = "" if default == "" else f"{DEFAULT_FORMAT}({default}){RESET} "
    if prompt != "":
        prompt += " "
    response = input(f"{prompt}{defaultText}> ")
    if response == "":
        response = default
    return response


def promptColor(promptText: str = "") -> str:
    lastColor = "f00000"
    while True:
        color = prompt(f"{promptText} (leave blank to confirm):")
        if color == "":
            break
        lastColor = color.lstrip('#').lower()
        if re.search(r"^[0-9a-f]{3}$", lastColor):
            lastColor = lastColor[0] + lastColor[0] + lastColor[1] + lastColor[1] + lastColor[2] + lastColor[2]
        if re.search(r"^[0-9a-f]{6}$", lastColor):
            r = int(lastColor[:2], 16)
            g = int(lastColor[2:4], 16)
            b = int(lastColor[4:], 16)
            avg = (r + g + b) / 3
            previewFg = fg.white
            if avg < 0.5:
                previewFg = fg.black
            say(f"- The color you input (if your terminal supports RGB colors): {bg.truecolor(r, g, b)}{previewFg}PREVIEW{fx.reset}. Enter blank to confirm")
        else:
            say(f"- Invalid hex color!")
    
    return '#' + lastColor

