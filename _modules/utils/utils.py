import re
from os import makedirs
from typing import Tuple


class IniMineyError(Exception):
    pass


def getFormat(path):
    return path[path.rfind('.') + 1:]


def hexToRgb(colorHex: str) -> Tuple[int, int, int]:
    colorHex = colorHex.lstrip('#')
    if re.search(r"^[0-9a-f]{3}$", colorHex):
        colorHex = colorHex[0] + colorHex[0] + colorHex[1] + colorHex[1] + colorHex[2] + colorHex[2]
    if re.search(r"^[0-9a-f]{6}$", colorHex):
        r = int(colorHex[:2], 16)
        g = int(colorHex[2:4], 16)
        b = int(colorHex[4:], 16)
        return r, g, b
    else:
        raise ValueError("Invalid hex color")


def rgbToHex(r: float, g: float, b: float):
    r, g, b = int(r), int(g), int(b)
    r, g, b = min(r, 255), min(g, 255), min(b, 255)
    r, g, b = max(r, 0), max(g, 0), max(b, 0)
    hr = hex(r)[2:]
    hg = hex(g)[2:]
    hb = hex(b)[2:]
    if len(hr) == 1:
        hr = '0' + hr
    if len(hg) == 1:
        hg = '0' + hg
    if len(hb) == 1:
        hb = '0' + hb
    return "#" + hr + hg + hb


def makeOutDir(outPath: str):
    makedirs(outPath[:outPath.rfind('/')], exist_ok=True)
