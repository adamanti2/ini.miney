from PIL import Image, ImageSequence
from typing import List, Literal, Optional, Tuple, Union
from math import floor, ceil


def resizeAndCrop1Frame(frame: Image.Image, resizeW: Optional[int] = None, resizeH: Optional[int] = None, cropW: Optional[int] = None, cropH: Optional[int] = None, resample: Image.Resampling = Image.Resampling.BICUBIC, cropOutward: bool = False) -> Image.Image:
    # Resizes and crops the individual frame in the image.
    resizedW: int
    if resizeW is not None:
        resizedW = resizeW
    elif resizeH is not None:
        resizedW = resizeH * frame.width // frame.height
    else:
        resizedW = frame.width
    
    resizedH: int
    if resizeH is not None:
        resizedH = resizeH
    elif resizeW is not None:
        resizedH = resizeW * frame.height // frame.width
    else:
        resizedH = frame.height
    
    resizedFrame = frame.resize((resizedW, resizedH), resample=resample)

    centerX = resizedFrame.width / 2
    centerY = resizedFrame.height / 2

    left = 0 if cropW is None else floor(centerX - cropW / 2)
    top = 0 if cropH is None else floor(centerY - cropH / 2)
    right = resizedFrame.width if cropW is None else floor(centerX + cropW / 2)
    bottom = resizedFrame.height if cropH is None else floor(centerY + cropH / 2)

    if not cropOutward:
        left = max(left, 0)
        top = max(left, 0)
        right = min(right, resizedFrame.width)
        bottom = min(bottom, resizedFrame.height)

    croppedFrame = resizedFrame.crop((left, top, right, bottom))

    return croppedFrame


def resizeThenCrop(

        original_img: Image.Image,
        resizeW: Optional[int] = None,
        resizeH: Optional[int] = None,
        cropW: Optional[int] = None,
        cropH: Optional[int] = None,
        resample: Image.Resampling = Image.Resampling.BICUBIC,
        cropOutward: bool = False,

    ) -> Union[
        Tuple[Literal[False], Image.Image],
        Tuple[Literal[True], Tuple[List[Image.Image], List[int]]]
    ]:

    img_w, img_h = (original_img.size[0], original_img.size[1])
    n_frames = original_img.n_frames

    # single frame image
    if n_frames == 1:
        return False, resizeAndCrop1Frame(original_img, resizeW=resizeW, resizeH=resizeH, cropW=cropW, cropH=cropH, resample=resample, cropOutward=cropOutward)
    # in the case of a multiframe image
    else:
        frames = []
        durations = []
        frame: Image.Image
        for frame in ImageSequence.Iterator(original_img):
            if 'duration' in frame.info:
                durations.append(frame.info['duration'])
            frames.append( resizeAndCrop1Frame(frame, resizeW=resizeW, resizeH=resizeH, cropW=cropW, cropH=cropH, resample=resample, cropOutward=cropOutward) )
        
        if original_img.format == "WEBP":
            durations.append(durations[-1])
        
        return True, (frames, durations)
