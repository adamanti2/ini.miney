from typing import Optional
from PIL import Image, ImageDraw


AO_ICON_SIZE = 40
AO_ICON_DIMS = (AO_ICON_SIZE, AO_ICON_SIZE)

def aoIconFrameBorder(color = 0):
    img = Image.new('RGBA', AO_ICON_DIMS, color)
    draw = ImageDraw.Draw(img)
    for outlineShape in (
        (0, 2, 1, 37),
        (2, 38, 37, 39),
        (38, 2, 39, 37),
        (2, 0, 37, 1),
        (1, 1, 2, 2),
        (1, 37, 2, 38),
        (37, 37, 38, 38),
        (37, 1, 38, 2),
    ):
        draw.rectangle(outlineShape, (0, 0, 0))
    return img


def aoIconFrameFill(color, colorLower = None):
    MARGIN = 2
    FILL_SIZE = AO_ICON_SIZE - MARGIN * 2
    FILL_DIMS = (FILL_SIZE, FILL_SIZE)
    base = Image.new('RGBA', FILL_DIMS, color)
    if colorLower is not None and colorLower != color:
        lower = Image.new('RGBA', FILL_DIMS, colorLower)
        gradientMask = Image.new('L', FILL_DIMS)
        maskData = []
        for y in range(FILL_SIZE):
            maskData.extend((int(255 * (y / FILL_SIZE)),) * FILL_SIZE)
        gradientMask.putdata(maskData)
        base.paste(lower, (0, 0), mask=gradientMask)
    base.putpixel((0, 0), 0)
    base.putpixel((FILL_SIZE - 1, 0), 0)
    base.putpixel((FILL_SIZE - 1, FILL_SIZE - 1), 0)
    base.putpixel((0, FILL_SIZE - 1), 0)

    img = Image.new('RGBA', AO_ICON_DIMS, 0)
    img.paste(base, (MARGIN, MARGIN))
    return img


def aoIconFrame(fillColor, fillColorLower = None, borderColor = 0):
    img = aoIconFrameBorder(borderColor)
    fill = aoIconFrameFill(fillColor, fillColorLower)
    img.paste(fill, mask=fill)
    return img
