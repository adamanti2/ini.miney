from PIL import Image
from os.path import isdir, isfile
from os import walk, makedirs
from multiprocessing import Pool
from ansi.colour import fg, bg, fx
from _modules.utils.config import getConfig
from _modules.utils import cli, rescale, utils


def getOutPathFromInPath(path: str, inDir: str, outDir: str):
    outPath = path
    if path[:len(inDir)] == inDir:
        outPath = path[len(inDir):].lstrip('/')
    outPath = outDir + "/" + outPath
    return outPath

def processOneImage(path: str, keepFormat: bool, inDir: str, outDir: str, quality: int):
    outPath = getOutPathFromInPath(path, inDir, outDir)
    format = outPath[outPath.rfind('.') + 1:]
    outPath = outPath[:outPath.rfind('.')]
    
    imageOrig = Image.open(path)
    animated, imageData = rescale.resizeThenCrop(imageOrig, resizeH=384, cropW=640)

    if not keepFormat:
        if animated:
            format = "webp"
        else:
            format = "png"
    outPath = outPath + "." + format
    makedirs(outPath[:outPath.rfind('/')], exist_ok=True)

    message = f"Resized {path}"
    if not animated:
        message += " (Not animated)"
        assert isinstance(imageData, Image.Image)
        imageData.save(outPath, format=format)
    else:
        message += " (Animated)"
        assert not isinstance(imageData, Image.Image)
        frames, durations = imageData
        frames[0].save(outPath, format=format, save_all=True, append_images=frames[1:], quality=quality, disposal=2, duration=durations)
    
    return message


def main():
    config = getConfig()
    outDir = config.imageOutputDir.rstrip('/')
    defaultInDir = config.defaultInDir
    
    inDir = cli.prompt("Enter input folder path", defaultInDir)
    while not isdir(inDir):
        inDir = cli.prompt(f"Folder '{inDir}' not found! Enter valid input folder", defaultInDir)
    inDir = inDir.rstrip('/')
    
    totalFiles = 0
    imagePaths = []
    alreadyExist = []
    maxNestedDirs = 0
    inDirFolders = inDir.count('/')
    for root, dirs, files in walk(inDir):
        
        root = root.rstrip('/')
        nestedDirs = root.count('/') - inDirFolders
        if nestedDirs > maxNestedDirs:
            maxNestedDirs = nestedDirs
        
        totalFiles += len(files)
        for filename in files:
            format = filename[filename.rfind('.') + 1:]
            if format in getConfig().olImageFormats:
                path = root + '/' + filename
                imagePaths.append(path)
                outPath = getOutPathFromInPath(path, inDir, outDir)
                if isfile(outPath):
                    alreadyExist.append(path)

    
    totalImageFiles = len(imagePaths)
    cli.say(f"Found {fx.underline}{totalImageFiles}{fx.reset} image files. ({totalFiles} total)")
    if maxNestedDirs > 4:
        cli.say(f"Warning: This path contains very deeply nested folders. Are you sure '{inDir}' is the right input directory?")
        if not cli.confirm(default=True):
            return
    
    if len(alreadyExist) > 0 and not getConfig().alwaysReplaceFiles:
        options = (("resizeall", "Replace all files"),)
        if len(alreadyExist) != totalImageFiles:
            options += (("resizenew", f"Resize the other {totalImageFiles - len(alreadyExist)} files"),)
        options += (("cancel", "Cancel resize"),)
        if len(alreadyExist) > 1:
            options += (("list", "List files"),)
        
        if len(alreadyExist) == 1:
            choice = cli.choose(f"Warning: The image {alreadyExist[0]} already exists in the output directory. Replace it?", options=options)
        elif len(alreadyExist) <= 3:
            fileNames = ", ".join(map(lambda path: path[path.rfind('/') + 1:], alreadyExist))
            choice = cli.choose(f"Warning: The images {fileNames} already exist in the output directory. Replace them?", options=options)
        else:
            choice = cli.choose(f"Warning: {len(alreadyExist)} of the images already exist in the output directory. Replace them?", options=options)
        
        while True:
            if choice == "resizeall":
                break
            elif choice == "resizenew":
                for path in alreadyExist:
                    imagePaths.remove(path)
                    totalImageFiles -= 1
                break
            elif choice == "cancel":
                cli.say("Cancelling resize.")
                return
            elif choice == "list":
                cli.say("List of file paths to be replaced:")
                for path in alreadyExist:
                    cli.say(" - " + path)
                choice = cli.choose(f"Replace {len(alreadyExist)} images?", options=options)
    
    
    cli.say(f"Resizing images...")
    config = getConfig()
    progress = 0
    def progressCallback(message: str):
        nonlocal progress
        progress += 1
        percentage = progress/totalImageFiles
        percentage = round(percentage * 10000) / 100
        percentage = "{:2.2f}".format(percentage)
        cli.say(f"{fg.darkgray}({progress}/{totalImageFiles}, {percentage}%){fx.reset} {message}")
    
    def errorCallback(error: BaseException):
        cli.errorMessage(f"Error while processing image: {error}", type(error))
    
    with Pool(processes=config.cpuCoresAllowed) as pool:
        results = []
        for path in imagePaths:
            validAOFormat = utils.getFormat(path) in getConfig().aoImageFormats
            result = pool.apply_async(processOneImage, (path, validAOFormat, inDir, outDir, config.webpQuality), callback=progressCallback, error_callback=errorCallback)
            results.append(result)
        for result in results:
            result.wait()
        cli.say("Done")
