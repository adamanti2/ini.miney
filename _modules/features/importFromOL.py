import json
from PIL import Image
from os.path import isdir, isfile
from os import makedirs
from multiprocessing import Pool
from objectionpy.enums import CharacterLocation
from objectionpy.assets import Character
from pydantic import BaseModel
from enum import Enum
from pydub import AudioSegment
from urllib.parse import unquote
from io import BytesIO
from colorsys import rgb_to_hsv, hsv_to_rgb
import warnings
import requests
import re
from ansi.colour import fg, bg, fx, rgb
from _modules.utils.config import getConfig
from _modules.utils import cli, rescale, utils, poseicons
from typing import Dict, List, Optional, Tuple, Union


class ImportConfig(BaseModel):
    characterID: int
    extraPoseIDs: List[int] = []

    folderName: str
    blipName: Optional[str] = None
    blip: Optional[str] = None
    iconStyle: str = "none"

    class BubbleData(BaseModel):
        custom: bool = False
        aoName: Optional[str] = None
        saveImage: Optional[bool] = None
        saveSound: Optional[bool] = None
    bubbles: Dict[str, BubbleData] = {}

    chatbox: str = "default"
    shouts: str = "default"
    scaling: str = "smooth"
    soundMap: Dict[str, str] = {}

    class IconData(BaseModel):
        gradientColorTop: str
        gradientColorBottom: str
    iconData: Optional[IconData] = None


def promptForID(first=True, message="character ID (leave blank to cancel)") -> Tuple[Optional[int], Optional[Character]]:
    id = cli.prompt(f"Enter {'' if first else 'valid '}{message}")
    if id == "":
        return None, None
    
    try:
        id = int(id)
        
        warnings.filterwarnings('ignore')
        char = Character(id)
        if not char.exists:
            cli.errorMessage(f"Error: Character ID {id} does not exist!")
            warnings.filterwarnings('default')
            return promptForID(False, message)
        warnings.filterwarnings('default')

        if len(char.poses) == 0:
            cli.errorMessage(f"Error: Character {char.name} doesn't have any poses!")
            return promptForID(False, message)
        
        return id, char
    
    except ValueError:
        return promptForID(False, message)


def blipAOfromOL(olBlipURL: str) -> Optional[str]:
    if olBlipURL == "/Audio/blip.wav":
        return "male"
    elif olBlipURL == "/Audio/blip-female.wav":
        return "female"
    elif olBlipURL == "/Audio/blip-machine.wav":
        return "typewriter"
    elif olBlipURL == "/Audio/blip-echo.wav":
        return "perceive"
    elif olBlipURL == "/Audio/blip-muted.wav":
        return "muted"
    else:
        return None


def findMSFrame(frameDurations: List[int], ms: int) -> Tuple[int, int]:
    frameNumber = -1
    prevTime = 0
    time = 0
    for frame, duration in enumerate(frameDurations):
        prevTime = time
        time += duration
        if time > ms:
            lo = ms - prevTime
            hi = time - ms
            if hi > lo:
                frameNumber = frame
            else:
                frameNumber = frame - 1
            return frameNumber, ms
    return frameNumber, time


def main():
    
    mode = cli.choose("Choose method of import", (("cancel", "Cancel import"), ("id", "Import by character ID"), ("importconfig", "Use (character)_import.json file")))

    id: Optional[int]
    char: Optional[Character]
    importConfig = None
    if mode == "id":

        while True:
            id, char = promptForID()
            if id is None or char is None:
                cli.say("\nCancelled import.")
                return
            
            cli.say(f"Import character '{char.name}' ({len(char.poses)} poses)?")
            if cli.confirm(default=True):
                break
    
    elif mode == "importconfig":
        
        jsonFile = ""
        while True:
            jsonFile = cli.prompt("Enter path to the import JSON file:")
            if jsonFile == "":
                cli.say("Cancelled import.")
                return
            if '.' not in jsonFile[jsonFile.rfind('/'):]:
                jsonFile = jsonFile + '.json'
            if jsonFile[jsonFile.rfind('.') + 1:] != 'json':
                cli.errorMessage("That's not a JSON file!")
                continue
            if not isfile(jsonFile):
                cli.errorMessage("File not found!")
                continue
            break
        
        with open(jsonFile, "r") as f:
            importConfig = ImportConfig.parse_raw(f.read())
        id = importConfig.characterID
        char = Character(id)
    
    else:
        cli.say("Cancelled import.")
        return
    
    poses = char.poses
    if mode == "id":

        cli.say("\n! Enter character folder name (no spaces, usually only either first or last name, e.g. 'PhoenixSOJ')")
        folderName = cli.prompt("  Folder name", default=re.sub(r"[^A-Za-z0-9\-_]", "", char.name))
        
        extraPoseIDs = []
        cli.say("\n! Would you like to mix other characters' poses into this character (useful for characters with multiple parts)")
        if cli.confirm("  -", default=False):
            if mode == "id":
                while True:
                    extraID, extraChar = promptForID(message="mix-in character's ID (leave blank to move on)")
                    if extraID is None or extraChar is None:
                        break
                    if cli.confirm(f"Mix in character '{extraChar.name}'s {len(extraChar.poses)} poses?", default=True):
                        extraPoseIDs.append(extraID)
                        poses.extend(extraChar.poses)

        hasIcons = False
        hasMissingIcons = False
        for pose in poses:
            if pose['iconUrl'] != '':
                hasIcons = True
            elif pose['iconUrl'] == '':
                hasMissingIcons = True
        
        if hasMissingIcons:
            cli.say("\n! Warning: This character has missing pose icons! Exporting it will cause missing pose buttons.")
            if not cli.confirm("  Continue anyway?"):
                return

        iconStyle = "none"
        iconData = None
        if hasIcons:
            iconStyle = cli.choose("\n! Select pose icon style", (("none", "Copy icons as they are"), ("framed", "Automatically frame pose icons AO-style")))
            if iconStyle == "framed":
                top = cli.promptColor("  - Enter the top gradient color for the icon frames")
                topH, topS, topV = rgb_to_hsv(*utils.hexToRgb(top))

                bottomColorOptions = ()
                for r, g, b in (
                    hsv_to_rgb(topH, max(0, topS - 0.12), min(255, topV + 50)),
                    hsv_to_rgb(topH, min(1, topS + 0.08), max(0, topV - 50)),
                    hsv_to_rgb(min(1, topH + 0.2), topS, min(255, topV + 15)),
                    hsv_to_rgb(max(0, topH - 0.2), topS, max(0, topV - 15)),
                ):
                    r, g, b = round(r), round(g), round(b)
                    hexColor = utils.rgbToHex(r, g, b)
                    if hexColor == top:
                        continue
                    avg = (r + g + b) / 3
                    previewFg = fg.white
                    if avg < 0.5:
                        previewFg = fg.black
                    bottomColorOptions += (
                        (hexColor, f"{bg.truecolor(r, g, b)}{previewFg}{hexColor.upper()}{fx.reset}"),
                    )

                if len(bottomColorOptions) == 0:
                    bottomChoice = "choose_manually"
                else:
                    bottomChoice = cli.choose("  - Choose bottom gradient color", bottomColorOptions + (("choose_manually", "Pick hex color manually"),))
                if bottomChoice == "choose_manually":
                    bottom = cli.promptColor("  - Enter the bottom gradient color for the icon frames")
                else:
                    bottom = bottomChoice
                iconData = ImportConfig.IconData(gradientColorTop=top, gradientColorBottom=bottom)

        aoBubbles = {}
        if len(char.bubbles) > 0:
            cli.say(f"\n! This character has {len(char.bubbles)} speech bubbles.")
        for bubble in char.bubbles:
            custom = False
            aoName = None
            saveImage = None
            saveSound = None
            bubbleChoice = cli.choose(f"  - Save '{bubble['name']}'?", (('none', "Don't save"), ('objection', 'As "Objection"'), ('holdit', 'As "Hold It"'), ('takethat', 'As "Take That"'), ('actuallycustom', 'As a custom shout'))) # ('custom', 'As the main "Custom Objection"'),
            if bubbleChoice == 'none':
                continue
            
            if bubbleChoice in ('objection', 'holdit', 'takethat', 'custom'):
                aoName = bubbleChoice
            else:
                custom = True
                aoName = cli.prompt('    - Enter speech bubble name:', default=bubble['name'])
            
            if cli.confirm('    - Save bubble sound?', default=True):
                saveSound = True
            if cli.confirm('    - Save bubble image?', default=True):
                saveImage = True
            
            aoBubbles[bubble['name']] = ImportConfig.BubbleData(custom=custom, aoName=aoName, saveImage=saveImage, saveSound=saveSound)

        scaling = cli.choose("\n! Select character scaling", (("smooth", "'smooth' (for higher-res characters)"), ("pixel", "'pixel' (for pixelated characters)")), 0)
        chatbox = cli.prompt(f"! (Optional) Enter character's default textbox name {fg.darkgray}(e.g. default, dd, dgs){fx.reset}")
        shouts = cli.prompt(f"! (Optional) Enter character's shouts style {fg.darkgray}(e.g. default, dd, dgs){fx.reset}")

        blip = None
        blipName = None
        if unquote(char.blipUrl.lower()) not in getConfig().aoSoundMap and blipAOfromOL(char.blipUrl) is None:
            cli.say(f"\n! The character has a custom blip URL: '{char.blipUrl}'")
            cli.say("  Does this sound effect already have a name in your AO client or should it be downloaded?")
            cli.say("  Enter existing blip sound effect name (examples: male, ddfemale) (if blank, blip will be downloaded):")
            blipSound = cli.prompt("  ")
            if blipSound != "":
                blip = blipSound
            else:
                blipName = cli.prompt("  What name should the blip be saved as?", default=unquote(char.blipUrl.lower()[char.blipUrl.rfind('/') + 1:char.blipUrl.rfind('.')]))

        uniqueSounds = []
        soundsFound = {}
        soundMap = {}
        for pose in poses:
            for audio in pose['audioTicks']:
                if unquote(audio['fileName'].lower()) in getConfig().aoSoundMap:
                    continue
                if audio['fileName'] in soundsFound:
                    continue
                soundsFound[audio['fileName']] = True
                uniqueSounds.append((audio['fileName'], pose['name']))
        if len(uniqueSounds) > 1:
            cli.say(f"\n! The poses of this character include {fx.bold}{fx.underline}{len(uniqueSounds)}{fx.reset} unidentified sounds.")
            cli.say(f"  If those sounds exist in AO, enter the AO sound names to map them to.")
            cli.say(f"  (Leave them blank to automatically download blip sound)")
            for sound, poseName in uniqueSounds:
                cli.say(f"\n  - Pose: {poseName}, sound URL: {sound}")
                mappedSound = cli.prompt("  Enter AO sound (or leave blank)")
                if mappedSound != "":
                    soundMap[sound] = mappedSound

        importConfig = ImportConfig(characterID=id, extraPoseIDs=extraPoseIDs, folderName=folderName, chatbox=chatbox, shouts=shouts, bubbles=aoBubbles, scaling=scaling, iconStyle=iconStyle, iconData=iconData, blip=blip, blipName=blipName, soundMap=soundMap)
    
    if importConfig is None:
        cli.errorMessage("Error: Failed to load import config!")
        return
    
    if mode not in ("id",):
        for id in importConfig.extraPoseIDs:
            extraChar = Character(id)
            if extraChar.exists:
                poses.extend(extraChar.poses)
            else:
                cli.errorMessage(f"Mix-in character {id} doesn't exist!")
    
    importCharacter(char, poses, importConfig)
    
    cli.say("Done")


class ImageType(Enum):
    IDLE = 0
    SPEECH = 1
    PRE = 2

def processPres(urls: List[str], outPath: str, emotion: str, imageType: ImageType, displayDurations: List[int], message: str, allowedFormats: List, quality: int) -> Tuple[str, str, ImageType, List[int]]:
    """outPath specified without file format"""
    totalFrames = []
    totalDurations = []
    format = None
    first = True
    w, h = 0, 0
    for i, url in enumerate(urls):
        response = requests.get(url)
        imageOrig = Image.open(BytesIO(response.content))
        if first:
            first = False
            animated, imageData = rescale.resizeThenCrop(imageOrig, resizeH=384, cropW=640)
            firstFrame = imageData
            if animated:
                assert not isinstance(imageData, Image.Image)
                frames, _ = imageData
                firstFrame = frames[0]
            assert isinstance(firstFrame, Image.Image)
            w, h = firstFrame.width, firstFrame.height
        else:
            animated, imageData = rescale.resizeThenCrop(imageOrig, resizeH=h, cropW=w, cropOutward=True)
        displayDuration = displayDurations[i]
        
        if animated:
            assert not isinstance(imageData, Image.Image)
            frames, durations = imageData
            time = 0
            for frameN, duration in enumerate(durations):
                time += duration
                totalFrames.append(frames[frameN])
                totalDurations.append(durations[frameN])
                if time > displayDuration:
                    break
            else:
                totalDurations[-1] += (displayDuration - time)
            format = url[url.rfind('.') + 1:]
        else:
            assert isinstance(imageData, Image.Image)
            totalFrames.append(imageData)
            totalDurations.append(displayDuration)
            if format is None:
                format = url[url.rfind('.') + 1:]
    
    if format is None:
        raise TypeError("Couldn't determine pre-anim format")
    
    utils.makeOutDir(outPath)
    if format not in allowedFormats:
        format = "webp"
    outPath += "." + format
    totalFrames[0].save(outPath, format=format, save_all=True, append_images=totalFrames[1:], quality=quality, disposal=2, duration=totalDurations)
    
    return f"Downloaded {message}", emotion, imageType, totalDurations


def processPoseImage(url: str, outPath: str, emotion: str, imageType: ImageType, message: str, allowedFormats: List, quality: int) -> Tuple[str, str, ImageType, List[int]]:
    """outPath specified without file format"""
    response = requests.get(url)
    imageOrig = Image.open(BytesIO(response.content))
    animated, imageData = rescale.resizeThenCrop(imageOrig, resizeH=384, cropW=640)

    format = url[url.rfind('.') + 1:]
    if format not in allowedFormats:
        if animated:
            format = "webp"
        else:
            format = "png"
    
    utils.makeOutDir(outPath)
    outPath += "." + format
    durations = []
    if animated:
        assert not isinstance(imageData, Image.Image)
        frames, durations = imageData
        frames[0].save(outPath, format=format, save_all=True, append_images=frames[1:], quality=quality, disposal=2, duration=durations)
    else:
        assert isinstance(imageData, Image.Image)
        imageData.save(outPath, format=format)
    
    return f"Downloaded {message}", emotion, imageType, durations


def simpleDownloadImage(url: str, width: Optional[int] = None, height: Optional[int] = None):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    if width is not None or height is not None:
        img = rescale.resizeAndCrop1Frame(img, width, height)

    return img

def downloadCharImage(url: str, outPath: str, message: str, allowedFormats: List, width: Optional[int], height: Optional[int]):
    """outPath specified without file format"""
    format = url[url.rfind('.') + 1:]
    if format not in allowedFormats:
        format = allowedFormats[0]
    utils.makeOutDir(outPath)
    
    if width is None and height is None:
        response = requests.get(url)
        with open(outPath + '.' + format, 'wb') as f:
            f.write(response.content)
    else:
        img = simpleDownloadImage(url, width, height)
        img.save(outPath + '.' + format, format=format)
    return f"Downloaded {message}"

def processPoseIcon(url: str, outPath: str, message: str, iconStyle: str, iconData: ImportConfig.IconData):
    """outPath specified without file format"""
    origImg = simpleDownloadImage(url, poseicons.AO_ICON_SIZE - 4, poseicons.AO_ICON_SIZE - 4)

    if iconStyle == "none":
        icon = origImg
    elif iconStyle == "framed":
        icon = poseicons.aoIconFrameBorder()
        fill = poseicons.aoIconFrameFill(iconData.gradientColorTop, iconData.gradientColorBottom)
        icon.paste(fill, mask=fill)

        char = Image.new('RGBA', poseicons.AO_ICON_DIMS, 0)
        char.paste(origImg, (2, 2))
        char.paste(char, mask=fill)
        icon.alpha_composite(char)
    else:
        raise ValueError(f"Unknown pose icon style '{iconStyle}'")

    utils.makeOutDir(outPath)
    icon.save(outPath + 'off.png', format='png')
    
    dim = Image.new("RGBA", poseicons.AO_ICON_DIMS, (0, 0, 0, 127))
    if iconStyle == "none":
        icon.alpha_composite(dim)
    elif iconStyle == "framed":
        dim.paste(dim, mask=icon)
        icon.alpha_composite(dim)

    icon.save(outPath + 'on.png', format='png')
    return f"Downloaded {message}"


def downloadCharSound(url: str, outPath: str, message: str, allowedFormats: List):
    """outPath specified without file format"""
    response = requests.get(url)
    soundBytes = response.content

    utils.makeOutDir(outPath)

    format = url[url.rfind('.') + 1:]
    if format not in allowedFormats:
        sound = AudioSegment.from_file(BytesIO(soundBytes), format=format)
        format = "wav"
        sound.export(outPath + '.' + format, format=format)
    else:
        with open(outPath + '.' + format, 'wb') as f:
            f.write(soundBytes)

    return f"Downloaded {message}"


def makeErrorCallback(text: str):
    def errorCallback(error: BaseException):
        cli.errorMessage(f"Error while {text}: {error}", type(error))
    return errorCallback


def importCharacter(char: Character, poses: List[Dict], importConfig: ImportConfig):
    cli.say("\n-- Converting --\n")

    config = getConfig()

    with open(f"{config.imageOutputDir.rstrip('/')}/{importConfig.folderName}_import.json", "w") as f:
        f.write(importConfig.json(exclude_none=True, indent=2, sort_keys=2))

    baseDir = f"{config.imageOutputDir.rstrip('/')}/"
    baseDir += f"{importConfig.folderName}/"
    baseDir += "base/"

    charDir = baseDir + "characters/" + importConfig.folderName + "/"

    with Pool(processes=config.cpuCoresAllowed) as pool:
        poolResults = []
        poolResultsFiles = {}

        processProgress = 0
        def progressCallback(message: str):
            nonlocal processProgress
            processProgress += 1
            totalProcesses = len(poolResults)
            percentage = processProgress/totalProcesses
            percentage = round(percentage * 10000) / 100
            percentage = "{:2.2f}".format(percentage)
            cli.say(f"{fg.darkgray}({processProgress}/{totalProcesses}, {percentage}%){fx.reset} {message}")

        imageDurations = {}
        def imageDownloadCallback(args):
            message, emotion, imageType, durations = args
            if emotion not in imageDurations:
                imageDurations[emotion] = {}
            imageDurations[emotion][imageType] = durations
            progressCallback(message)

        def startDownloadProcess(fileType: str, func, argsList, errorText: str, callback=progressCallback):
            outPath = argsList[1]
            if fileType + outPath in poolResultsFiles:
                pass
            else:
                result = pool.apply_async(func, argsList, callback=callback, error_callback=makeErrorCallback(errorText))
                poolResultsFiles[fileType + outPath] = result
                poolResults.append(result)

        # Start constructing .ini file
        iniOptions = "[Options]\n"
        iniOptions += f"name = {importConfig.folderName}\n"
        if char.namePlate != ".":
            iniOptions += f"showname = {char.namePlate}\n"
        else:
            iniOptions += f"showname = {char.name}\n"
            iniOptions += f"needs_showname = false\n"
        
        print(char.side)
        if char.side in (CharacterLocation.DEFENSE, "defense"):
            iniOptions += f"side = def\n"
        elif char.side in (CharacterLocation.PROSECUTION, "prosecution"):
            iniOptions += f"side = pro\n"
        elif char.side in (CharacterLocation.COUNSEL, "counsel"):
            iniOptions += f"side = hld\n"
        elif char.side in (CharacterLocation.JUDGE, "judge"):
            iniOptions += f"side = jud\n"
        else:
            iniOptions += f"side = wit\n"
        print(iniOptions)
        
        if importConfig.blip is not None:
            iniOptions += f"blips = {importConfig.blip}\n"
        elif unquote(char.blipUrl.lower()) in config.aoSoundMap:
            iniOptions += f"blips = {config.aoSoundMap[unquote(char.blipUrl.lower())]}\n"
        else:
            blip = blipAOfromOL(char.blipUrl)
            if blip is not None:
                iniOptions += f"blips = {blip}\n"
            else:
                soundName = importConfig.blipName or importConfig.folderName
                iniOptions += f"blips = {soundName}\n"
                startDownloadProcess(
                    "sound",
                    downloadCharSound,
                    (char.blipUrl, f"{baseDir}/sounds/blips/{soundName}", "blip sound", config.aoSoundFormats),
                    "downloading blip sound"
                )
        
        if char.iconUrl is not None and char.iconUrl != "":
            startDownloadProcess(
                "image",
                downloadCharImage,
                (char.iconUrl, f"{charDir}/char_icon", "character icon", ("png",), 60, 60),
                "downloading character icon"
            )

        if importConfig.chatbox != "":
            iniOptions += f"chat = {importConfig.chatbox}\n"
        if importConfig.shouts != "":
            iniOptions += f"shouts = {importConfig.shouts}\n"
        if importConfig.scaling != "":
            iniOptions += f"scaling = {importConfig.scaling}\n"
        iniOptions += "\n"


        iniShouts = ""
        for bubble in char.bubbles:
            if bubble['name'] not in importConfig.bubbles:
                continue
            bubbleData = importConfig.bubbles[bubble['name']]

            bubbleDir = charDir
            if bubbleData.custom:
                bubbleDir += "custom_objections/"
                makedirs(bubbleDir, exist_ok=True)

            if bubbleData.saveSound:
                startDownloadProcess(
                    "sound",
                    downloadCharSound,
                    (bubble['soundUrl'], f"{bubbleDir}{bubbleData.aoName}", f"shout sound ({bubble['name']})", ("wav",)),
                    f"downloading shout sound for '{bubble['name']}'"
                )
            if bubbleData.saveImage:
                startDownloadProcess(
                    "image",
                    downloadCharImage,
                    (bubble['imageUrl'], f"{bubbleDir}{bubbleData.aoName}", f"shout image ({bubble['name']})", config.aoImageFormats, None, None),
                    f"downloading shout image for '{bubble['name']}'"
                )
            
            if bubbleData.custom and bubbleData.aoName is not None:
                iniShouts += f"{bubbleData.aoName}_Name={bubbleData.aoName}\n"
                exclam = "" if bubbleData.aoName[-1] == "!" else "!"
                iniShouts += f"{bubbleData.aoName}_Message={bubbleData.aoName.upper()}{exclam}\n"
        if len(iniShouts) > 0:
            iniShouts = "[Shouts]\n" + iniShouts + "\n"


        iniEmotions = "[Emotions]\n"
        emotionLines = []
        soundNLines = []
        animLines = []
        idleLines = []
        speechLines = []
        msEffects = []

        emotionNumber = 1
        for pose in poses:
            name = pose['name']
            key = name.lower()
            key = re.sub(r'\W+', '_', key).strip('_')
            key = re.sub(r'[^a-zA-Z0-9_\-]', '', key)

            hasPre = len(pose['states']) > 0
            preAnim = "-"
            if hasPre:
                preAnim = "anim/" + key
            
            emote = "/" + key

            modifier = 0
            hasPreOrEffects = hasPre or len(pose['audioTicks']) > 0 or len(pose['functionTicks']) > 0
            if hasPreOrEffects:
                if pose['isSpeedlines']:
                    modifier = 6
                else:
                    modifier = 1
            else:
                if pose['isSpeedlines']:
                    modifier = 5
                else:
                    modifier = 0
            
            line = f"{emotionNumber} = {name}#{preAnim}#{emote}#{modifier}"

            if pose['isSpeedlines']:
                line += "#0"

            for audio in pose['audioTicks']:
                url = audio['fileName']
                if url in importConfig.soundMap:
                    soundName = importConfig.soundMap[url]
                elif unquote(url.lower()) in config.aoSoundMap:
                    soundName = config.aoSoundMap[unquote(url.lower())]
                else:
                    
                    fileName = unquote(url[url.rfind('/') + 1 : url.rfind('.')])
                    soundName = f"{importConfig.folderName}/{fileName}"
                    startDownloadProcess(
                        "sound",
                        downloadCharSound,
                        (url, f"{baseDir}/sounds/general/{soundName}", f"sound '{fileName}' (of pose '{pose['name']}')", config.aoSoundFormats),
                        f"downloading '{pose['name']} sound"
                    )

                if audio['time'] > 0:
                    msEffects.append((key, audio['time'], f"{key}_FrameSFX", soundName))
                elif hasPre:
                    animLines.append(f'{key}_FrameSFX\\1 = {soundName}')
                else:
                    soundNLines.append(f'{emotionNumber} = {soundName}')
            
            for effect in pose['functionTicks']:
                if effect['functionName'].lower() == 'flash':
                    effectName = 'Realization'
                elif effect['functionName'].lower() == 'shake':
                    effectName = 'Screenshake'
                else:
                    continue
                if effect['time'] > 0:
                    msEffects.append((key, effect['time'], f"{key}_Frame{effectName}", 1))
                elif hasPre:
                    animLines.append(f'{key}_Frame{effectName}\\1 = 1')
            
            preURLs = []
            preDurations = []
            for pre in pose['states']:
                if len(preURLs) > 0 and preURLs[-1] == pre['imageUrl']:
                    preDurations[-1] += pre['nextPoseDelay']
                else:
                    preURLs.append(pre['imageUrl'])
                    preDurations.append(pre['nextPoseDelay'])
            if len(preURLs) > 0:
                startDownloadProcess(
                    "image",
                    processPres,
                    (preURLs, f"{charDir}anim/{key}", key, ImageType.PRE, preDurations, f"pose '{pose['name']}' pre-anim", config.aoImageFormats, config.webpQuality),
                    f"downloading '{pose['name']}'s pre-anim",
                    callback=imageDownloadCallback,
                )
            
            for urlKey, folder, messageWord, imageType in (('idleImageUrl', '(a)', 'idle', ImageType.IDLE), ('speakImageUrl', '(b)', 'speaking', ImageType.SPEECH)):
                if pose[urlKey] == "":
                    urlKey = 'idleImageUrl'
                startDownloadProcess(
                    "image",
                    processPoseImage,
                    (pose[urlKey], f"{charDir}{folder}/{key}", key, imageType, f"pose '{pose['name']}' {messageWord} image", config.aoImageFormats, config.webpQuality),
                    f"downloading '{pose['name']}'s {messageWord} image",
                    callback=imageDownloadCallback,
                )

            if 'iconUrl' in pose and pose['iconUrl'] != '':
                startDownloadProcess(
                    "image",
                    processPoseIcon,
                    (pose['iconUrl'], f"{charDir}emotions/button{emotionNumber}_", f"icon for '{pose['name']}'", importConfig.iconStyle, importConfig.iconData),
                    f"downloading pose icon for '{pose['name']}'",
                )
            
            emotionLines.append(line)
            emotionNumber += 1
        
        iniEmotions += f"number = {len(emotionLines)}\n"
        iniEmotions += "\n".join(emotionLines) + "\n"

        if importConfig.iconStyle == "framed" and isinstance(importConfig.iconData, ImportConfig.IconData):
            iconBase = poseicons.aoIconFrame(importConfig.iconData.gradientColorTop, importConfig.iconData.gradientColorBottom)
            makedirs(f"{charDir}emotions", exist_ok=True)
            iconBase.save(f"{charDir}emotions/Base{importConfig.folderName}.png")
        

        for result in poolResults:
            result.wait()

        for emotion, milliseconds, iniKey, iniValue in msEffects:
            try:
                frameNumber = None
                if emotion in imageDurations:
                    if ImageType.PRE not in imageDurations[emotion]:
                        frameNumber = -1
                        time = 0
                    else:
                        frameNumber, time = findMSFrame(imageDurations[emotion][ImageType.PRE], milliseconds)
                    
                    if frameNumber > -1:
                        line = f"{iniKey}\\{frameNumber + 1} = {iniValue}"
                        animLines.append(line)
                    else:
                        idleFrameNumber, _ = findMSFrame(imageDurations[emotion][ImageType.IDLE], milliseconds - time)
                        if idleFrameNumber > -1:
                            idleLines.append(f"{iniKey}\\{idleFrameNumber + 1} = {iniValue}")
                        else:
                            cli.errorMessage(f"Effect '{iniKey}' for '{emotion}' emotion is out of range for (a)")
                        speechFrameNumber, _ = findMSFrame(imageDurations[emotion][ImageType.SPEECH], milliseconds - time)
                        if speechFrameNumber > -1:
                            speechLines.append(f"{iniKey}\\{speechFrameNumber + 1} = {iniValue}")
                        else:
                            cli.errorMessage(f"Effect '{iniKey}' for '{emotion}' emotion is out of range for (b)")
            except Exception as error:
                cli.errorMessage(f"Error while adding '{iniKey}' effect: {error}", type(error))


        iniSoundN = ""
        if len(soundNLines) > 0:
            iniSoundN = "[SoundN]\n"
            soundNLines.sort()
            iniSoundN += '\n'.join(soundNLines) + '\n'
        iniAnim = ""
        if len(animLines) > 0:
            iniAnim = "[anim]\n"
            animLines.sort()
            iniAnim += '\n'.join(animLines) + '\n'
        iniIdle = ""
        if len(idleLines) > 0:
            iniIdle = "[(a)]\n"
            idleLines.sort()
            iniIdle += '\n'.join(idleLines) + '\n'
        iniSpeech = ""
        if len(speechLines) > 0:
            iniSpeech = "[(b)]\n"
            speechLines.sort()
            iniSpeech += '\n'.join(speechLines) + '\n'

        sections = (iniOptions, iniShouts, iniEmotions, iniSoundN, iniAnim, iniIdle, iniSpeech)
        sections = filter(lambda section: section != "", sections)
        ini = "\n".join(sections)

        with open(f"{charDir}char.ini", "w") as f:
            f.write(ini)
