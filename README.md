# ini.miney

A tool to convert between different Ace Attorney chat room character formats (Attorney Online, Objection.Lol)

## To use:

- Install Python 3.10.
- Open the command line in the ini.miney folder and type `pip install -r requirements.txt`
- Run the script with `py .`
- Use the instructions in your command line to navigate
