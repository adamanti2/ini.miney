from ansi.colour import fg, bg, fx
from _modules.features import downscaleOL, importFromOL
from _modules.utils.config import getConfig, writeConfig
from _modules.utils import cli, utils


def main():
    startup = True
    with writeConfig():
        pass
    while True:
        if startup:
            startup = False
            cli.say("\nini.miney - an Attorney Online <-> Objection.Lol character converter\n")
        else:
            cli.padding(2)
        
        action = cli.choose("Main menu:", (
            ("exit", "Exit ini.miney"),
            ("downscale-for-ao", f"{fg.darkgray}(OL \u2192 AO){fx.reset} Downscale sprites for AO"),
            ("import-from-ol", f"{fg.darkgray}(OL \u2192 AO){fx.reset} Import full character from OL"),
        ))
        cli.padding()

        try:

            if action == "exit":
                break
            elif action == "downscale-for-ao":
                downscaleOL.main()
            elif action == "import-from-ol":
                importFromOL.main()
        
        except Exception as e:
            errorType = type(e)
            if errorType is utils.IniMineyError:
                errorType = None
            cli.errorMessage(f"Error: {e}", type(e))


if __name__ == "__main__":
    main()
